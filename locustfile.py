from locust import  task, between,HttpUser

class WebsiteTestUser(HttpUser):
    wait_time = between(1, 5)

    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        pass

    def on_stop(self):
        """ on_stop is called when the TaskSet is stopping """
        pass


    @task
    def sayHi(self):
        self.client.get("api/intent.sentence=Hi")
