import spacy  # type: ignore
import uvicorn # type: ignore
import logging
import os
from fastapi import FastAPI, HTTPException

logging.info("Loading model..")
nlp = spacy.load("./models")

app = FastAPI()


@app.get("/api/intent/")
async def intent_inference(sentence):
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return inference.cats


@app.get("/api/intent-supported-languages")
async def supported_languages():
    return ["fr-FR"]


@app.get("/health")
async def health_check_endpoint():
    raise HTTPException(200)


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=int(os.environ.get('PORT', 8080)), debug=True, reload=True)
