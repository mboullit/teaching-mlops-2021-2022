# Group members

- Hamza Benmendil
- Houssam Bahhou
- Mohamed Faycal Boullit

# Benchmark

### Vos services sont vachement gourmand coté ML. Tu peux me fournir la taille de ton image docker et la limite de RAM à définir sur les environnements stp ?
L'image fait une taille de 1.19GB. (Inclus les librairies python)

![weight](Benchmark_img/weight.png)

Pour la RAM, l'image utilise environ 762.1MB et donc on peut définir une limite de RAM de 1GB.

![ram](Benchmark_img/ram.png)

### Le modèle est viable jusqu’à quel trafic en production ? On souhaite avoir un P99 < 200ms?
Pour répondre à cette problèmatique nous avons utilisé Locust, cet outil permet de faire un monitoring simple et efficace de notre application. 
Nous avons testé notre modèle pour différents nombres d'utilisateurs concurrents.
Malheureusement pour avoir un P99 < 200ms, il fallait réduire le nombre d'utilisateur à 1 avec un traffic de 0.5 requête par seconde. (voir les 2 figures suivantes)

![locust1](Benchmark_img/p9905.jpg)
![locust2](Benchmark_img/p9905-rps.jpg)

Cependant pour un traffic de 350 requêtes par seconde (1000 utilisateurs), on obtient P99 <= 1700. Les figures ci-dessous illustrent ces résultats:

![locust3](Benchmark_img/p993.jpg)
![locust4](Benchmark_img/p993-rps.jpg)
![locust5](Benchmark_img/p993-rt.jpg)

### On veut enrichir de la donnée dans la stack data avec ton modèle mais ça rame fort, t'as des idées pour améliorer les perfs?
Afin d'améliorer les perfs on peut traiter des batchs de requêtes et envoyer le résultat de chaque batch à l'utilisateur, au lieu de les traiter d'une manière séquentielle.
