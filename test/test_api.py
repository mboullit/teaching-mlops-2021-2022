import unittest
from fastapi import FastAPI
from fastapi.testclient import TestClient
import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from main import app

client = TestClient(app)

class TestApi(unittest.TestCase):

    def test_intent_inference(self):
        response = client.get("/api/intent/?sentence=Tapis")
        assert response.status_code == 200
        assert list(response.json().keys()) == ['purchase', 'find-restaurant', 'irrelevant', 'find-hotel', 'provide-showtimes', 'find-around-me', 'find-train', 'find-flight']
        assert list(filter(lambda x: x < 0 or x > 1, response.json().values())) == []

    def test_intent_inference_no_args(self):
        response = client.get("/api/intent/")
        assert response.status_code == 422
        assert response.json()['detail'][0]['msg'] == 'field required'
        
    def test_supported_languages(self):
        response = client.get("/api/intent-supported-languages")
        assert response.status_code == 200
        assert response.json() == ['fr-FR']
        
    def test_health_check_endpoint(self):
        response = client.get("/health")
        assert response.status_code == 200

if __name__ == "__main__":
    unittest.main()